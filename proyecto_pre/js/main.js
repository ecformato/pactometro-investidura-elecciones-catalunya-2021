import '../css/main.scss';

let barraSi = document.getElementById('barra-sies'),
    barraNo = document.getElementById('barra-noes'),
    barraPartidos = document.getElementsByClassName('barra--empty-parties')[0],
    candidateSelector = document.getElementById('candidateSelection'),
    candidateOptions = candidateSelector.getElementsByTagName('option'),
    btnOptions = document.getElementsByClassName('btn'),
    reInitBtn = document.getElementById('reinit'),
    textResult = document.getElementById('text-update'),
    currentCandidate = 'psc',
    totalYes = 0,
    totalAbstencion = 0,
    totalNo = 0,
    total = 135;

let partyDiccionary = {
    psc: {name: 'psc', escanos: 35, color: '#f60b01'},
    erc: {name: 'erc', escanos: 29, color: '#f9b33c'},
    jxc: {name: 'jxc', escanos: 26, color: '#20c0b2'},
    cs: {name: 'cs', escanos: 15, color: '#f45a09'},
    cup: {name: 'cup', escanos: 10, color: '#fff200'},
    pp: {name: 'pp', escanos: 8, color: '#48aafd'},
    podem: {name: 'podem', escanos: 6, color: '#692772'},
    vox: {name: 'vox', escanos: 6, color: '#77be2d'}
}

initPartyBars();
setFirstDraw('psc');
setTimeout(function() {
    setFirstBars('psc');
}, 100); 

/*
* EVENTO en SELECTOR DE CANDIDATOS 
*/
candidateSelector.addEventListener('change', function(e){
    let index = e.target.options.selectedIndex;
    currentCandidate = candidateOptions[index].value;
    reInitBtn.style.display = 'none';
    reInit();
});

/*
* EVENTO en BOTONES DE PARTIDOS 
*/
for(let i = 0; i < btnOptions.length; i++) {
    btnOptions[i].addEventListener('click', function(e){
        setNewData(e.target)
    });
}

/*
* EVENTO en BOTÓN DE REINICIO
*/
reInitBtn.addEventListener('click', function() {
    this.style.display = 'none';
    reInit();    
});


/*
*
* LÓGICA
*
*/
function initPartyBars() {
    Object.entries(partyDiccionary).forEach(function(item,index) {
        let itemBarraSi = document.createElement('div');
        itemBarraSi.style.background = item[1].color;
        itemBarraSi.classList.add(`barra-sies`,`barra-${item[1].name}`);
        let itemBarraNo = document.createElement('div');
        itemBarraNo.style.background = item[1].color;
        itemBarraNo.classList.add(`barra-noes`,`barra-${item[1].name}`);        

        barraPartidos.appendChild(itemBarraSi);
        barraPartidos.appendChild(itemBarraNo);
    });
}

function setFirstDraw(candidato) {
    
    //Propio partido
    let bloquePartidos = document.getElementsByClassName('party--block');
    let bloqueConcreto = document.getElementsByClassName(`party--block-${candidato}`)[0];

    //Todos en blanco
    for(let i = 0; i < btnOptions.length; i++) {
        if(btnOptions[i].classList.contains('selected')){
            btnOptions[i].classList.remove('selected');
        }
    }

    for(let i = 0; i < bloquePartidos.length; i++) {
        let item = bloquePartidos[i];

        if(item != bloqueConcreto) {
            let btnAbst = item.children[2];
            btnAbst.classList.add('selected');
        } else {
            let btnYes = bloqueConcreto.children[1];
            btnYes.classList.add('selected');
        }
    }
}

function setFirstBars(candidato) {
    //Barra genérica
    totalYes += partyDiccionary[candidato].escanos;
    barraSi.style.width = (totalYes * 100) / total + '%';

    //Barra partidos
    let currentParty = document.getElementsByClassName(`barra-${candidato}`)[0];
    currentParty.style.width = (partyDiccionary[candidato].escanos * 100) / total + '%';
}

function setNewData(btnSelected) {
    //Dibujo de botones
    let cssPartido = btnSelected.classList.item(2);
    let botonesPartido = document.getElementsByClassName(`${cssPartido}`);

    let currentSelected = '', splitCurrentSelected = '';
    //Cuál es el que está seleccionado para este partido
    for(let i = 0; i < botonesPartido.length; i++) {
        if(botonesPartido[i].classList.contains('selected')){
            currentSelected = botonesPartido[i].id;
        }
    }

    //Quitamos información al total de votos
    splitCurrentSelected = currentSelected.split('-');
    let escanos = partyDiccionary[splitCurrentSelected[0]].escanos;

    if(splitCurrentSelected[1] == 'yes') {
        totalYes -= escanos;
    } else if(splitCurrentSelected[1] == 'no') {
        totalNo -= escanos;
    }

    //Botón actual >>>>>> Se puede mejorar la lógica
    for(let i = 0; i < botonesPartido.length; i++) {
        botonesPartido[i].classList.remove('selected');
    }
    btnSelected.classList.add('selected');

    let splitId = btnSelected.id.split('-');

    if(splitId[1] == 'no'){
        totalNo += escanos;
    } else if(splitId[1] == 'yes'){
        totalYes += escanos;
    }

    totalAbstencion = total - (totalYes + totalNo);

    //Dibujo de barras
    barraSi.style.width = (totalYes * 100) / total + '%';
    barraNo.style.width = (totalNo * 100) / total + '%';

    let itemBarra = document.getElementsByClassName(`barra-${splitId[0]}`);
    
    if(splitId[1] == 'no'){
        if(parseInt(itemBarra[0].style.width) != 0){
            itemBarra[0].style.width = '0%';
        }
        itemBarra[1].style.width = (partyDiccionary[splitId[0]].escanos * 100) / total + '%';
    } else if(splitId[1] == 'yes'){
        if(parseInt(itemBarra[1].style.width) != 0){
            itemBarra[1].style.width = '0%';
        }
        itemBarra[0].style.width = (partyDiccionary[splitId[0]].escanos * 100) / total + '%';
    } else {
        if(parseInt(itemBarra[0].style.width) != 0){
            itemBarra[0].style.width = '0%';
        }
        if(parseInt(itemBarra[1].style.width) != 0){
            itemBarra[1].style.width = '0%';
        }
    }

    //Otros
    reInitBtn.style.display = 'block';
    setCandidateText();
}

function setCandidateText(){
    if(totalYes > total / 2) {
        textResult.textContent = `Ganaría con mayoría absoluta con ${totalYes} síes, ${totalNo} noes y ${totalAbstencion} abstenciones`;
    } else if (totalYes > totalNo) {
        textResult.textContent = `Ganaría con mayoría simple en segunda votación con ${totalYes} síes, ${totalNo} noes y ${totalAbstencion} abstenciones`;
    } else {
        textResult.textContent = `No saldría investido candidato en ninguna votación con ${totalYes} síes, ${totalNo} noes y ${totalAbstencion} abstenciones`;
    }
}

function reInit() {
    //Variables internas
    totalYes = 0;
    totalAbstencion = 0;
    totalNo = 0;

    //Barras
    barraSi.style.width = '0%';
    barraNo.style.width = '0%';

    let barritas = barraPartidos.getElementsByTagName('div');
    for(let i = 0; i < barritas.length; i++) {
        barritas[i].style.width = '0%';
    }

    //Textos
    textResult.textContent = 'Pulse sobre los botones para ver quién ganaría';

    //Seteo de nuevos datos con nuevo candidato
    setFirstDraw(currentCandidate);
    initPartyBars(currentCandidate);
    setTimeout(function(){
        setFirstBars(currentCandidate);
    }, 2100); //Espera a la transición CSS de anchos de barras >> Diferenciar lógica y pintado de barras
}